#!/bin/bash
# pushes and publishes artifacts at aptly

# Parameters:
#  DEB_FILE   – build/target/awesome.deb
#  DEB_NAME   – awesome-server-master-1234
#  APTLY      – https://aptly.example.com
#  APTLY_PW   – f545g44742
#  APTLY_REPO – indy-snapshots

echo "Pushing ${DEB_FILE} as ${DEB_NAME} to ${APTLY}"

curl -u ci:${APTLY_PW} -X POST --header "Content-Type:multipart/form-data" -F file=@${DEB_FILE} ${APTLY}/files/${DEB_NAME}
curl -u ci:${APTLY_PW} -X POST ${APTLY}/repos/${APTLY_REPO}/file/${DEB_NAME}
curl -u ci:${APTLY_PW} -X PUT -H 'Content-Type: application/json' --data '{}' "${APTLY}/publish/:./indy"