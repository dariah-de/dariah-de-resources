#!/bin/bash
# creates the checksums files

#cleanup
rm -rf checksums.tsv;
rm -rf checksums.json;

for FILE in $(find . -type f)
do
  echo -ne "${FILE}\t" >> checksums.tsv
  shasum -b -a 384 $FILE | xxd -r -p | base64 >> checksums.tsv
done

# create JSON
echo "{" > checksums.json
sed 's/^\.\//"/' checksums.tsv | sed 's/\t/":\ "/' | sed 's/$/",/' | sed '$s/,//' \
 >> checksums.json
echo "}" >> checksums.json
