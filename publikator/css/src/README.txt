LESS (http://www.lesscss.de/) is used to add a prefix to the CSS files.

If you want to change CSS properties, please edit the corresponding less file.

Then execute compile.sh

You need to have LESS installed, which can be obtained via NODE:

npm install less