#!/bin/bash

lessc application.less ../application.css
lessc font-awesome.less ../font-awesome.css
lessc bootstrap-customization.less ../bootstrap-customization.css
lessc bootstrap-modal.less ../bootstrap-modal.css
lessc bootstrap-responsive.less ../bootstrap-responsive.css
lessc bootstrap.less ../bootstrap.css
lessc jstree-customization.less ../jstree-customization.css
lessc main.less ../main.css
lessc upload.less ../upload.css
lessc jstree-theme-default/style.less ../jstree-theme-default/style.css
lessc jstree-theme-default/style.min.less ../jstree-theme-default/style.min.css